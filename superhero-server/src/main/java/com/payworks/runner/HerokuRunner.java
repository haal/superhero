package com.payworks.runner;
import java.io.File;

import fish.payara.micro.BootstrapException;
import fish.payara.micro.PayaraMicro;

public class HerokuRunner {

	public static void main(String[] args) throws BootstrapException {
		PayaraMicro.getInstance().setHttpPort(Integer.parseInt(System.getenv("PORT")))
								 .addDeploymentFile(new File("superhero-rest/target/superhero.war"))
								 .bootStrap();
	}

}
