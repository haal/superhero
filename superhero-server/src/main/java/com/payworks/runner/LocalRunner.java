package com.payworks.runner;

import fish.payara.micro.BootstrapException;
import fish.payara.micro.PayaraMicro;

public class LocalRunner {

	public static void main(String[] args) throws BootstrapException {
		PayaraMicro.getInstance().addDeployment("../superhero-rest/target/superhero.war")
								 .bootStrap();
	}

}
