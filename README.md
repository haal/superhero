# superhero app

A superhero Java app, which can easily be deployed to Heroku.

## Used frameworks

* JEE7 web-api
* payara-micro (with default datasource - derby)

## Running Locally

Make sure you have Java and Maven installed.

	$ mvn clean install
	$ cd superhero-server
	$ mvn exec:exec

Your app should now be running on [localhost:8080](http://localhost:8080/).

## Endpoints

* GET <base-path>/superhero/api/superhero (get all superheroes)
* POST <base-path>/superhero/api/superhero (create superhero, use dummy GET for sample)
* GET <base-path>/superhero/api/superhero/{id}/dummy (sample of superhero)
* GET <base-path>/superhero/api/superhero/{id} (get superhero by id)


## Deploying to Heroku

	$ git remote add heroku git@heroku.com:<your-app-heroku-name>.git
	$ git push heroku master

