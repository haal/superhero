package com.payworks.superhero.boundary;

import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.payworks.superhero.entity.Superhero;

@Stateless
public class SuperheroService {
	
	@PersistenceContext
	private EntityManager em;
	
	public List<Superhero> all() {
        return this.em.createNamedQuery(Superhero.FIND_ALL, Superhero.class).getResultList();
    }

	public void create(Superhero superhero) {
		em.merge(superhero);
	}
	
	public Superhero createDummy(final long id) {
		Superhero res = new Superhero();
        res.setId(id);
        res.setPublisher("DC Commics");
        res.setName("Batman");
        res.setPseudonym("The Dark Knight");
        res.setAllies(Arrays.asList("Robin", "Tim Drake", "Barbara Gordon"));
        res.setPowers(Arrays.asList("Acrobatics", "Weaponry", "Stealth"));
        res.setFirstAppearance(new GregorianCalendar(1939, 3, 30).getTime());
        return res;
	}

	public Superhero findById(long id) {
		return em.find(Superhero.class, id);
	}

}
