package com.payworks.superhero.boundary;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.payworks.superhero.entity.Superhero;

@Stateless
@Path("/superhero")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SuperheroResource {
	
	@Inject
	SuperheroService service;
	
	@POST
	public void create(Superhero superhero) {
		service.create(superhero);
	}
	
	@GET
	public List<Superhero> all() {
		return service.all();
	}

	@GET
	@Path("{id}")
	public Superhero findById(@PathParam("id") long id) {
		return service.findById(id);
	}
	
	@GET
    @Path("{id}/dummy")
    public Superhero dummy(@PathParam("id") long id) {
        return service.createDummy(id);
    }

}
