package com.payworks.superhero.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.payworks.DateAdapter;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "superhero")
@NamedQuery(name = Superhero.FIND_ALL, query = "SELECT s FROM Superhero s")
public class Superhero {
	
	public static final String FIND_ALL = "Superhero.findAll";
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	private String name;
	private String pseudonym;
	private String publisher;
	
	@ElementCollection
	@CollectionTable(name = "superhero_power", joinColumns = {@JoinColumn(name="superhero_id")})
	private List<String> powers = new ArrayList<>();
	
	@ElementCollection
	@CollectionTable(name = "superhero_allie", joinColumns = {@JoinColumn(name="superhero_id")})
	private List<String> allies = new ArrayList<>();
	
	@Column(name = "first_appearance")
	@Temporal(TemporalType.DATE)
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date firstAppearance;
	
	public Superhero() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPseudonym() {
		return pseudonym;
	}

	public void setPseudonym(String pseudonym) {
		this.pseudonym = pseudonym;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public List<String> getPowers() {
		return powers;
	}

	public void setPowers(List<String> powers) {
		this.powers = powers;
	}

	public List<String> getAllies() {
		return allies;
	}

	public void setAllies(List<String> allies) {
		this.allies = allies;
	}

	public Date getFirstAppearance() {
		return firstAppearance;
	}

	public void setFirstAppearance(Date firstAppearance) {
		this.firstAppearance = firstAppearance;
	}
	
}
