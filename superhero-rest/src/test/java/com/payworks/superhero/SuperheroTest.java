package com.payworks.superhero;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.payworks.superhero.entity.Superhero;


public class SuperheroTest {
	
    EntityManager em;
    EntityTransaction tx;
    
    @Before
    public void init() {
        this.em = Persistence.createEntityManagerFactory("test").createEntityManager();
        this.tx = this.em.getTransaction();
    }
    
    @After
    public void close() {
    	em.close();
    }
    
    @Test
    public void testSave() {
    	Superhero superhero = new Superhero();
    	superhero.setName("batman");
    	tx.begin();
		em.merge(superhero);
		tx.commit();
    }


}
